stages:
    - cleanup
    - sast-scan
    - sca-scan
    - build
    - image-scan
    - run
    - dast-scan
    - push

variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
    API_DOCKER_IMAGE_NAME: "${CI_REGISTRY_IMAGE}:api"
    APP_DOCKER_IMAGE_NAME: "${CI_REGISTRY_IMAGE}:app"
    API_CONTAINER_NAME: "${CI_PROJECT_NAME}-api"
    APP_CONTAINER_NAME: "${CI_PROJECT_NAME}-app"

cleanup:
    stage: cleanup
    script:
        - docker stop $API_CONTAINER_NAME || true
        - docker stop $APP_CONTAINER_NAME || true

sonarcloud-check:
    stage: sast-scan
    image:
        name: sonarsource/sonar-scanner-cli:latest
        entrypoint: [""]
    cache:
        key: "${CI_JOB_NAME}"
        paths:
        - .sonar/cache
    script:
        - docker run --rm -u $(id -u):$(id -g) -e SONAR_HOST_URL="https://sonarcloud.io" -e SONAR_LOGIN="$SONAR_TOKEN" -v "$(pwd):/usr/src" sonarsource/sonar-scanner-cli

snyk-check-api:
    stage: sca-scan
    script:
        - docker run --rm --env SNYK_TOKEN=$SNYK_TOKEN -v $(pwd)/api:/app snyk/snyk:node
    allow_failure: true

snyk-check-app:
    stage: sca-scan
    script:
        - docker run --rm --env SNYK_TOKEN=$SNYK_TOKEN -v $(pwd)/app:/app snyk/snyk:node
    allow_failure: true

build-api:
    stage: build
    script:
        - docker build -t $API_DOCKER_IMAGE_NAME . -f Dockerfile-Api

build-app:
    stage: build
    script:
        - docker build -t $APP_DOCKER_IMAGE_NAME . -f Dockerfile-App

trivy-check-api:
    stage: image-scan
    image:
        name: bitnami/trivy:latest
        entrypoint: [""]
    script:
        - trivy image $API_DOCKER_IMAGE_NAME --scanners vuln

trivy-check-app:
    stage: image-scan
    image:
        name: bitnami/trivy:latest
        entrypoint: [""]
    script:
        - trivy image $APP_DOCKER_IMAGE_NAME --scanners vuln

run:
    stage: run
    script:
        - docker run --rm --name $API_CONTAINER_NAME -d -p 3030:3030 $API_DOCKER_IMAGE_NAME
        - docker run --rm --name $APP_CONTAINER_NAME -d -p 3000:3000 $APP_DOCKER_IMAGE_NAME

owasp-zap-check-api:
    stage: dast-scan
    script:
        - export API_CONTAINER_IP_ADDRESS=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $API_CONTAINER_NAME)
        - docker run --rm -t owasp/zap2docker-stable zap-baseline.py -t http://$API_CONTAINER_IP_ADDRESS:3030
    allow_failure: true

owasp-zap-check-app:
    stage: dast-scan
    script:
        - export APP_CONTAINER_IP_ADDRESS=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $APP_CONTAINER_NAME)
        - docker run --rm -t owasp/zap2docker-stable zap-baseline.py -t http://$APP_CONTAINER_IP_ADDRESS:3000
    allow_failure: true

push-images:
    stage: push
    script:
        - docker login registry.gitlab.com -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
        - docker push $API_DOCKER_IMAGE_NAME
        - docker push $APP_DOCKER_IMAGE_NAME