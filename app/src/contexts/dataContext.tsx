import {IData} from "../interfaces/IData";
import {createContext, useCallback, useMemo, useState} from "react";
import {getAllDatas} from "../services/datasApi";
import {IDepartement} from "../interfaces/IDepartement";

interface IDataContext {
    datas: IData[];
    setDatas: any;
    DATAS_YEARS: string[];
    DATAS_REGIONS: string[];
    DATAS_DEPARTEMENTS: IDepartement[];
}

export const DataContext = createContext<IDataContext>({
    datas: [],
    setDatas: () => {},
    DATAS_YEARS: [],
    DATAS_REGIONS: [],
    DATAS_DEPARTEMENTS: []
});

const initialState: {data: IData[]} = await getAllDatas();

export const DataContextProvider = (props: any) => {
    const [datas, setDatas] = useState(initialState.data);
    const memoizedSetDatas = useCallback(setDatas, []);
    const DATAS_YEARS: string[] = useMemo(() => [...new Set(datas.map(data => data.annee_publication))].sort((a, b) => Number(b) - Number(a)).filter(Boolean), [datas]);
    const DATAS_REGIONS: string[] = useMemo(() => [...new Set(datas.map(data => data.nom_region))].sort((a, b) => a.localeCompare(b)).filter(Boolean), [datas]);
    const DATAS_DEPARTEMENTS: IDepartement[] = useMemo(() => datas.map(data => ({nom_region: data.nom_region, nom_departement: data.nom_departement}))
        .reduce((acc: IDepartement[], current) => {
            if (current.nom_departement && current.nom_region) {
                const x = acc.find(item => (item.nom_departement === current.nom_departement && item.nom_region === current.nom_region));
                if (!x) {
                    return acc.concat([current]);
                }
            }
            return acc;
        }, [])
        .sort((a, b) => a.nom_departement.localeCompare(b.nom_departement)), [datas]);

    return (
        <DataContext.Provider value={{datas, setDatas: memoizedSetDatas, DATAS_YEARS, DATAS_REGIONS, DATAS_DEPARTEMENTS}}>
            {props.children}
        </DataContext.Provider>
    );
}