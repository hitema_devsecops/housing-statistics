import React, {useContext} from 'react';
import './App.css';
import {DataContext} from "./contexts/dataContext";
import DatasTable from "./components/DatasTable";
import DatasTableFilters from "./components/DatasTableFilters";
import DatasChartsFilter from "./components/DatasChartsFilter";
import DatasCharts from "./components/DatasCharts";
import {SELECTION_OF_DATA_PROPERTIES_TO_FILTER} from "./helpers/constants";

function App() {
    const {DATAS_YEARS} = useContext(DataContext);
    const [chartsYear, setChartsYear] = React.useState<string>(DATAS_YEARS[0]);
    const [tableFilters, setTableFilters] = React.useState<{}>(Object.fromEntries(SELECTION_OF_DATA_PROPERTIES_TO_FILTER.map(key => [key, key.includes('_comparator_type') ? 'inférieur' : ''])));

    return (
        <div className="App">
            <header className="App-header">
                <h1>Statistiques des logements</h1>
            </header>
            <div>
                <h2>Graphiques</h2>
                <DatasChartsFilter chartsYear={chartsYear} setChartsYear={setChartsYear} />
                <DatasCharts chartsYear={chartsYear} />
            </div>
            <hr />
            <div>
                <h2>Tableau de données</h2>
                <DatasTableFilters tableFilters={tableFilters} setTableFilters={setTableFilters} />
                <DatasTable tableFilters={tableFilters} />
            </div>
        </div>
    );
}

export default App;
