import {IData} from "../interfaces/IData";

export const groupDataByRegion = (dataArray: IData[], valueKey: string) => {
    return dataArray.reduce((acc: { [key: string]: number }, data) => {
        if (!acc[data.nom_region]) {
            acc[data.nom_region] = Number(data[valueKey]);
        } else {
            acc[data.nom_region] += Number(data[valueKey]);
        }
        return acc;
    }, {});
}

export const weightedAverageByRegion = (dataArray: IData[], valueKey: string, weightKey: string) => {
    const sums = dataArray.reduce((acc: { [key: string]: { sum: number, weight: number } }, data) => {
        if (!acc[data.nom_region]) {
            acc[data.nom_region] = { sum: Number(data[valueKey]) * Number(data[weightKey]), weight: Number(data[weightKey]) };
        } else {
            acc[data.nom_region].sum += Number(data[valueKey]) * Number(data[weightKey]);
            acc[data.nom_region].weight += Number(data[weightKey]);
        }
        return acc;
    }, {});

    const averages = Object.keys(sums).reduce((acc: { [key: string]: number }, region) => {
        acc[region] = sums[region].sum / sums[region].weight;
        return acc;
    }, {});

    return averages;
}