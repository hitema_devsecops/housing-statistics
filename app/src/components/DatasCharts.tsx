import React, {useContext} from "react";
import {DataContext} from "../contexts/dataContext";
import {groupDataByRegion, weightedAverageByRegion} from "../helpers/manipulateDatas";
import DatasBarChart from "./DatasBarChart";

interface DatasChartsProps {
    chartsYear: string,
}

const DatasCharts: React.FC<DatasChartsProps> = ({chartsYear}) => {
    const {datas} = useContext(DataContext);
    const chartsDatas = datas.filter(data => data.annee_publication === chartsYear);
    const groupedDataByRegionInhabitants = groupDataByRegion(chartsDatas, 'nombre_d_habitants');
    const groupedDataByUnemploymentRate = weightedAverageByRegion(chartsDatas, 'taux_de_chomage_au_t4_en', 'nombre_d_habitants');
    const groupedDataByRegionConstruction = groupDataByRegion(chartsDatas, 'moyenne_annuelle_de_la_construction_neuve_sur_10_ans');

    const chartsDataByRegionInhabitants = Object.keys(groupedDataByRegionInhabitants)
        .sort((a, b) => a.localeCompare(b))
        .map(region => ({
            nom_region: region,
            nombre_d_habitants: groupedDataByRegionInhabitants[region]
        }));

    const chartsDataByRegionUnemploymentRate = Object.keys(groupedDataByUnemploymentRate)
        .sort((a, b) => a.localeCompare(b))
        .map(region => ({
            nom_region: region,
            taux_chomage: groupedDataByUnemploymentRate[region]
        }));

    const chartsDataByRegionConstruction = Object.keys(groupedDataByRegionConstruction)
        .sort((a, b) => a.localeCompare(b))
        .map(region => ({
            nom_region: region,
            nombre_construction: groupedDataByRegionConstruction[region]
        }));

    return (
        <div className="charts">
            <div className="width-50">
                <h3>Nombre d'habitants par région en {chartsYear}</h3>
                <DatasBarChart data={chartsDataByRegionInhabitants} xAxisDataKey="nom_region" barDataKey="nombre_d_habitants" />
            </div>
            <div className="width-50">
                <h3>Nombre de construction par région en {chartsYear}</h3>
                <DatasBarChart data={chartsDataByRegionConstruction} xAxisDataKey="nom_region" barDataKey="nombre_construction" />
            </div>
            <div  className="width-100">
                <h3>Taux de chômage par région en {chartsYear}</h3>
                <DatasBarChart data={chartsDataByRegionUnemploymentRate} xAxisDataKey="nom_region" barDataKey="taux_chomage" />
            </div>
        </div>
    );
}

export default DatasCharts;