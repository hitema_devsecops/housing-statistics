import React, {useContext} from "react";
import {DataContext} from "../contexts/dataContext";
import {
    DATA_PROPERTIES_NICE_NAMES,
    ITEMS_PER_PAGE_OPTIONS,
    SELECTION_OF_DATA_PROPERTIES_TO_DISPLAY
} from "../helpers/constants";
import {IData} from "../interfaces/IData";

interface DatasTableProps {
    tableFilters: {[key: string]: string},
}

const DatasTable: React.FC<DatasTableProps> = ({tableFilters}) => {
    const {datas} = useContext(DataContext);
    const tableDatas = datas.filter(data =>
        Object.entries(tableFilters).every(([filterKey, filterValue]) => {
            if (filterValue === "") {
                return true;
            }

            const baseKey = filterKey.replace('_comparator_type', '');
            const dataValue = data[baseKey];
            const comparisonOperator = tableFilters[`${baseKey}_comparator_type`];
            if (comparisonOperator) {
                if (tableFilters[baseKey] !== "" && !isNaN(Number(filterValue)) && !isNaN(Number(dataValue))) {
                    if (comparisonOperator === 'inférieur') {
                        return Number(dataValue) <= Number(filterValue);
                    } else if (comparisonOperator === 'supérieur') {
                        return Number(dataValue) >= Number(filterValue);
                    }
                } else {
                    return true;
                }
            }

            return String(dataValue).toLowerCase().includes(filterValue.toLowerCase());
        })
    );
    const [currentPage, setCurrentPage] = React.useState(1);
    const [itemsPerPage, setItemsPerPage] = React.useState(10);
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = tableDatas.slice(indexOfFirstItem, indexOfLastItem);

    const handleClickNext = () => {
        setCurrentPage(currentPage + 1);
    };

    const handleClickPrevious = () => {
        setCurrentPage(currentPage - 1);
    };

    return (
        <>
            <p>Nombre de lignes : {tableDatas.length}</p>
            <p>
                <label htmlFor="itemsPerPage">Nombre de lignes par page : </label>
                <select id="itemsPerPage" value={itemsPerPage} onChange={event => setItemsPerPage(Number(event.target.value))}>
                    {
                        ITEMS_PER_PAGE_OPTIONS.map((option: number) => (
                            <option key={window.crypto.randomUUID()} value={option}>{option}</option>
                        ))
                    }
                </select>
            </p>
            <table className="table">
                <thead>
                <tr>
                    {
                        SELECTION_OF_DATA_PROPERTIES_TO_DISPLAY.map((key: string) => (
                            <th key={window.crypto.randomUUID()}>{DATA_PROPERTIES_NICE_NAMES[key]}</th>
                        ))
                    }
                </tr>
                </thead>
                <tbody>
                {currentItems.map((data: IData) => (
                    <tr key={window.crypto.randomUUID()}>
                        {
                            SELECTION_OF_DATA_PROPERTIES_TO_DISPLAY.map((key: string) => (
                                <td key={window.crypto.randomUUID()}>{data[key]}</td>
                            ))
                        }
                    </tr>
                ))}
                </tbody>
            </table>
            <button className="pagination-button" onClick={handleClickPrevious} disabled={currentPage === 1}>Précédent</button>
            <button className="pagination-button" onClick={handleClickNext} disabled={currentPage === Math.ceil(tableDatas.length / itemsPerPage)}>Suivant
            </button>
        </>
    );
}

export default DatasTable;