import {Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";

interface BarChartProps {
    data: any[],
    xAxisDataKey: string,
    barDataKey: string,
}

const DatasBarChart: React.FC<BarChartProps> = ({data, xAxisDataKey, barDataKey}) => {
    return (
        <ResponsiveContainer width="100%" height={350}>
            <BarChart width={1500} height={300} data={data} margin={{top: 0, right: 40, bottom: 150, left: 40}}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey={xAxisDataKey} angle={-75} fontSize="0.75rem" dx={-10} dy={75}/>
                <YAxis/>
                <Tooltip/>
                <Bar dataKey={barDataKey} fill="#8884d8"/>
            </BarChart>
        </ResponsiveContainer>
    );
}

export default DatasBarChart;