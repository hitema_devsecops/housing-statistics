import React, {useContext} from "react";
import {DataContext} from "../contexts/dataContext";
import {DATA_PROPERTIES_NICE_NAMES} from "../helpers/constants";
import {IDepartement} from "../interfaces/IDepartement";

interface DatasFiltersProps {
    tableFilters: {[key: string]: string}
    setTableFilters: (filters: {}) => void,
}

const DatasTableFilters: React.FC<DatasFiltersProps> = ({tableFilters, setTableFilters}) => {
    const {DATAS_YEARS, DATAS_REGIONS, DATAS_DEPARTEMENTS} = useContext(DataContext);
    const filteredDepartements = tableFilters.nom_region
        ? DATAS_DEPARTEMENTS.filter(departement => departement.nom_region === tableFilters.nom_region)
        : DATAS_DEPARTEMENTS;

    const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const {name, value} = event.target;

        setTableFilters({...tableFilters, [name]: value});
    }

    return (
        <form>
            <p>
                <label htmlFor="annee_publication">{DATA_PROPERTIES_NICE_NAMES.annee_publication} :</label>
                <select id="annee_publication" name="annee_publication"
                        value={tableFilters.annee_publication} onChange={handleChange}>
                    <option value="">Toutes</option>
                    {
                        DATAS_YEARS.map((year: string) => <option key={window.crypto.randomUUID()} value={year}>{year}</option>)
                    }
                </select>
            </p>
            <p>
                <label htmlFor="nom_region">{DATA_PROPERTIES_NICE_NAMES.nom_region}: </label>
                <select id="nom_region" name="nom_region"
                        value={tableFilters.nom_region} onChange={handleChange}>
                    <option value="">Toutes</option>
                    {
                        DATAS_REGIONS.map((region: string) => <option key={window.crypto.randomUUID()} value={region}>{region}</option>)
                    }
                </select>
            </p>
            <p>
                <label htmlFor="nom_departement">{DATA_PROPERTIES_NICE_NAMES.nom_departement} :</label>
                <select id="nom_departement" name="nom_departement"
                        value={tableFilters.nom_departement} onChange={handleChange}>
                    <option value="">Tous</option>
                    {
                        filteredDepartements.map((departement: IDepartement) => <option key={window.crypto.randomUUID()} value={departement.nom_departement}>{departement.nom_departement}</option>)
                    }
                </select>
            </p>
            <p>
                <label htmlFor="taux_de_chomage_au_t4_en">{DATA_PROPERTIES_NICE_NAMES.taux_de_chomage_au_t4_en} :</label>
                <select id="taux_de_chomage_au_t4_en_comparator_type" name="taux_de_chomage_au_t4_en_comparator_type"
                        value={tableFilters.taux_de_chomage_au_t4_en_comparator_type} onChange={handleChange}>
                    <option value="inférieur">inférieur à</option>
                    <option value="supérieur">supérieur à</option>
                </select>
                <input type="number" id="taux_de_chomage_au_t4_en" name="taux_de_chomage_au_t4_en"
                       value={tableFilters.taux_de_chomage_au_t4_en} onChange={handleChange}/>
                %
            </p>
            <p>
                <label htmlFor="taux_de_pauvrete_en">{DATA_PROPERTIES_NICE_NAMES.taux_de_pauvrete_en} :</label>
                <select id="taux_de_pauvrete_en_comparator_type" name="taux_de_pauvrete_en_comparator_type"
                        value={tableFilters.taux_de_pauvrete_en_comparator_type} onChange={handleChange}>
                    <option value="inférieur">inférieur à</option>
                    <option value="supérieur">supérieur à</option>
                </select>
                <input type="number" id="taux_de_pauvrete_en" name="taux_de_pauvrete_en"
                       value={tableFilters.taux_de_pauvrete_en} onChange={handleChange}/>
                %
            </p>
        </form>
    );
}

export default DatasTableFilters;