import React, {useContext} from "react";
import {DataContext} from "../contexts/dataContext";

interface ChartsFilterProps {
    chartsYear: string,
    setChartsYear: (year: string) => void,
}

const DatasChartsFilter: React.FC<ChartsFilterProps> = ({chartsYear, setChartsYear}) => {
    const {DATAS_YEARS} = useContext(DataContext);

    return (
        <form>
            <p>
                <label htmlFor="chartsYear">Année :</label>
                <select id="chartsYear" name="chartsYear"
                        value={chartsYear} onChange={(event) => setChartsYear(event.target.value)}>
                    {
                        DATAS_YEARS.map((year, index) => <option key={index} value={year}>{year}</option>)
                    }
                </select>
            </p>
        </form>
    );
}

export default DatasChartsFilter;