export interface IDatasFilters {
    annee_publication: string,
    nom_region: string,
    nom_departement: string,
}