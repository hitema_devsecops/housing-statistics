import axios, {AxiosInstance} from 'axios';

/* Axios instance for API call */
const HTTP: AxiosInstance = axios.create({
    baseURL: 'http://localhost:3030/',
});

const AXIOS_RESPONSE_TITLE: string = 'API request response :';
const AXIOS_ERROR_TITLE: string = 'API request error :';
const UNEXPECTED_ERROR_TITLE: string = 'Unexpected error :';

/* Functions for each routes */
export async function getAllDatas() {
    try {
        const response = await HTTP.get('/datas');
        console.log(AXIOS_RESPONSE_TITLE, response.data);
        return response.data;
    } catch (error) {
        handleError(error)
    }
}

/* Other functions */
const handleError = (error: any) => {
    if (axios.isAxiosError(error)) {
        console.error(AXIOS_ERROR_TITLE, error);
        return error.response;
    } else {
        console.error(UNEXPECTED_ERROR_TITLE, error);
        return 'An unexpected error occurred';
    }
}