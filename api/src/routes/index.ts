import express from 'express';
import datasRoutes from './datasRoutes';

const routes = express.Router();

routes.get('', (req, res) => {
    res.send('Connected to API.')
});
routes.use('/datas', datasRoutes);

export default routes;