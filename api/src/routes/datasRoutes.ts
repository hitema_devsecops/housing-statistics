import express from 'express';
import DataController from '../controllers/DataController';

const datasRoutes = express.Router();

datasRoutes.get('', DataController.getAllDatas);

export default datasRoutes;