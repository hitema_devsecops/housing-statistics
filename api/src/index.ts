import express from 'express';
import cors from 'cors';
import routes from "./routes";

const app = express();
app.disable('x-powered-by');

const corsOptions = {
    origin: 'http://localhost:3000'
};
app.use(cors(corsOptions));

app.use(express.json());

app.use(routes);

const PORT = process.env.PORT || 3030;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});