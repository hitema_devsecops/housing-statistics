import fs from "fs";
import Papa from "papaparse";

export const readCsv = async (filepath: string): Promise<any> => {
    const csvFile = fs.readFileSync(filepath, 'utf8');
    return new Promise(resolve => {
        Papa.parse(csvFile, {
            header: true,
            complete: (results: any) => {
                resolve(results.data);
            }
        });
    });
}