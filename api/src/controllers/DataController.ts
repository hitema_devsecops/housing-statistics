import {Request, Response} from 'express';
import {httpInternalServerError, httpOk} from '../services/httpResponsesService';
import {IData} from "../models/IData";
import {readCsv} from "../helpers/csv-converter";

class DataController {
    static getAllDatas = async (req: Request, res: Response) => {
        try {
            const csvFilePath: string = './data/logements-et-logements-sociaux-dans-les-departements.csv';

            const datas: IData[] = await readCsv(csvFilePath);

            httpOk(res, datas);
        } catch (error) {
            console.error('Erreur lors de la récupération des données :', error);
            httpInternalServerError(res, 'Erreur lors de la récupération des données');
        }
    }

    // static searchStations = async (req: Request, res: Response) => {
    //     const searchQuery: string = req.body.query;
    //
    //     try {
    //         const result = await db.ft.search(
    //             'idx:stations',
    //             searchQuery,
    //             {
    //                 LIMIT: {
    //                     from: 0,
    //                     size: 100
    //                 }
    //             }
    //         )
    //
    //         let stations = [];
    //         for (const station of result.documents) {
    //             stations.push({id: station.id, ...station.value });
    //         }
    //
    //         httpOk(res, stations);
    //     } catch (error) {
    //         console.error('Erreur lors de la recherche de stations :', error);
    //         httpInternalServerError(res, 'Erreur lors de la recherche de stations');
    //     }
    // }
}

export default DataController;